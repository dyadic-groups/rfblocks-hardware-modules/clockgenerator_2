# A Low Jitter Variable Rate Clock Generator to 1000 MHz

The requirements for this clock generator were originally driven by a need
to supply a AD9913 DDS with a good quality clock signal in the 250 MHz range.
The design presented here should also be useful in other applications
where a low jitter clock source is required up to 1000 MHz.

## Features

- Good jitter and phase noise performance with RMS jitter being < 0.5 ps.
- Produces a selection of CMOS, LVDS, LVPECL clock signals.
- Agile frequency selection.  The AD9552 is based on a fractional-N
  phase locked loop design with the intention of replacing high frequency
  crystal oscillators and resonators.
- Preset output frequency selection using on board switch
  configuration.
- Reference switchable between on-board and external sources.
- Full configurability of output signal characteristics is available via
  software control.
- Up to four clock outputs with CMOS clock signals or two clock outputs
  with LVPECL or LVDS.
- Single supply operation with low power (< 400 mW in most cases)

## Documentation

Full documentation for the clock generator is available at 
[RF Blocks](https://rfblocks.org/boards/AD9552-ClockSource.html)

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
